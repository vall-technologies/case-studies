# Saint Vrain Valley Schools' Private 4G network

The [Saint Vrain Valley School District](https://www.svvsd.org/) has partnered with [NextLight](https://mynextlight.com/) and [Vall Technologies](https://valltechnologies.com/) to deploy a private 4G LTE network providing internet connectivity to over 4,000 students.

## Bridging the digital divide

Student success in the digital age is dependent on access to a high-quality and stable internet connection. The emergence of COVID-19 and the rapid shift to online and remote learning since 2020 has made the need for connectivity even more acute. Despite the proliferation of broadband internet, there are still large populations of students who are unable to afford reliable internet access or who live in areas not served by local ISPs. These students do not have the same opportunities to access information and resources when access is more important than ever.

To help bridge this divide, St. Vrain Valley Schools [received a $1.3 million grant](https://www.svvsd.org/2021/02/03/st-vrain-valley-schools-awarded-1-3-million-grant-in-partnership-with-the-city-of-longmonts-nextlight-program-to-expand-quality-internet-access-for-low-income-students/) from the Colorado Department of Education to deploy a private LTE network capable of providing high-quality internet access to low-income and unserved students.

## Deploying private 4G LTE across the city

To provide reliable internet access to the more than 4,000 qualifying students, St. Vrain Valley Schools and the City of Longmont's publicy-owned ISP Nextlight partnered with Vall Technologies to deploy a city-wide private LTE network using CBRS.

After a careful design and mapping phase, Vall Technologies deployed **25 (?)** LTE sites throughout the city to provide coverage for qualifying households. The team built the LTE sites atop district schools and city buildings using Baicells Nova 436Q eNBs paired with KP Performance antennas.

**(picture here)**

The network was certified by the NTIA in the fall of 2022 and, together with Vall Technologies' [deployment for the City of Longmont](https://gitlab.com/vall-technologies/valltech-svvsd/-/blob/main/cityoflongmont.md), is the largest student broadband and smart cities deployment using CBRS in the state of Colorado.

**(map of base stations and coverage?)**

Students will connect to the network using district-provided **_____ LTE** modems (and **_____ antennas**). This model is advantageous because instead of connecting each end-user with a single mobile device and SIM card, the network can provide connectivity to a single modem which serves an entire household wherein multiple students may reside.

The next stage of the project involves deployment of a further **(how many?)** LTE base stations **(of what kind?)** as Vall Technologies and St. Vrain Valley Schools continue to expand the coverage area.

## Managing the network

As St. Vrain Valley Schools continues to grow, the LTE network will need to expand and grow with it. Vall Technologies has designed the CBRS network and control plane to be expandable and reconfigurable without the need for a complete redesign or reimplementation.

**(what should we say about how we manage this network? Mention CloudCore?)**