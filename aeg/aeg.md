# AEG Presents

AEG Presents is a premier event promotion company with concerts, festivals and events at some of the best venues in the United States. Many of these events take place at venues where internet access is not immediately available - large fields, open lots and auditoriums with a lot of foot traffic where a wired network simply does not make sense. In summer of 2022, AEG partnered with Vall Technologies to implement wireless networks at large outdoor events.

* to facilitate ticket sales and scanning

* providing internet access to outside vendors who require internet access to make sales and do business at the event.

* provide guests with a separate wifi network so they can access their tickets and participate in events.

---

## P2P and P2MP wireless in-a-box

To get connectivity in these places, Vall Tech deploys a wireless point-to-multipoint network using either an off-site backhaul (if available) and/or StarLink sattellite backhaul for remote locations and failover connections.

The network is then broadcast by a Mimosa N5-360 Beamforming antenna to different sites and client devices - usually a Mimosa modular C5x - throughout the venue.

From these client devices, Vall Tech can deploy multiple WiFi access points throughout the venue without running any wiring across crowded spaces and with minimal setup.

---

## On-site support

From delivery trucks knocking down poles to unexpected power losses, unexpected issues can and will arise at live events, so Vall Technologies provides support with staff on-site throughout. Our on-site event staff can quickly address connection issues and assist vendors with network connectivity throughout the entirety of the show.

---

## Starlink & Deployment without a backhaul

---